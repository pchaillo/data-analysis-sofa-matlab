function print_dist_pres(tab,nom)

%# write line-by-line
fid = fopen(strcat(nom,'.csv'),'wt');
for i=1:length(tab)
    fprintf(fid, '%d,%d\n', tab(i,1),tab(i,2));
end
fclose(fid);