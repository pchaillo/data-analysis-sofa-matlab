function data_struct = csv_lecture(nom_csv)

chemin_csv = choix_chemin('record',nom_csv);
% Chargement du fichier csv
fiche = fopen(chemin_csv) ;

% Récupération des informations de ces fichiers
out = textscan(fiche,'%s%f','delimiter',',');

%Remise des informations dans des variables
Parametres = out{1, 2} ;
hauteur_module = Parametres(2);
init_pressure = Parametres(3);
young_soft = Parametres(4);
young_stiff = Parametres(5);
coef_poisson = Parametres(6);
nb_module = Parametres(7);
nb_cavit = Parametres(8);
nb_poutre = Parametres(9);
max_pressure = Parametres(10);
masse_module = Parametres(11);

format_txt = "%s%s"; % un s position, l'autre temps
for i = 1 : nb_module
    format_txt = format_txt + '%s%s' %un s pression, l'autre volume
end
    
out2 = textscan(fiche,format_txt,'delimiter',',');  

% Récupérations des tableaux
Positions_raw = out2{1,1}(4:end);
Positions = clean_cell_to_tab(Positions_raw);

Temps_raw = out2{1,2}(4:end);
Temps = clean_cell_to_tab(Temps_raw);

ind = 0;
for i = 1 : 2 : nb_module*2
    ind = ind + 1 ;
    Pressions_raw{ind} = out2{1,i+2}(4:end);
    Pressions{ind} = clean_cell_to_tab(Pressions_raw{ind});
    
    Volumes_raw{ind} = out2{1,i+3}(4:end);
    Volumes{ind} = clean_cell_to_tab(Volumes_raw{ind});
end

data_struct = {Parametres, Positions, Temps, Pressions, Volumes};