%% Positioning comparison :

% 200kPa 
beam_1cav = [36.34 0.0004 32.14];
fem_1cav = [36.13 0.0028 31.99];
cav1 = distance(beam_1cav,fem_1cav)

% 100kPa
beam_3cav = [1.11e-6 1.98e-6 57.21];
fem_3cav = [0.151 0.147 56.53];
cav3 = distance(beam_3cav,fem_3cav)

% 200kPa
beam_2cav = [19.51 33.783 36.1189];
fem_2cav = [20.64 36.2986  28.53 ];
cav2 = distance(beam_2cav,fem_2cav)

% 150kPa
beam_2cav_bis = [15.7387 27.2540 43.3055];
fem_2cavb_bis = [16.0620 27.9114  41.7409];
cav2_bis = distance(beam_2cav_bis,fem_2cavb_bis)


%% CLEAN (BEAM + FEM)

mod_100_t = [4.28416, 4.29371, 4.16808, 4.25172];
mod_1000_t = [41.6709  37.7266 37.3556 36.5354];
mod_100_fps = [23.3418 23.2899 23.9884 23.5199];
mod_1000_fps = [23.9976 26.5065 26.7697 27.3722];

mod2_100_t = [7.69151 7.04673 7.38002 7.45231];
mod2_1000_t = [71.02885 71.31632 72.60071 72.09432];
mod2_100_fps = [14.0881 14.1912 13.5501 13.4187];
mod2_1000_fps = [14.0788 14.022 13.774 13.8707];




%% FULL FEM

FE_1mod_100_t = [19.0933 18.7892 18.9843 17.8444];
FE_1mod_1000_t = [179.401 180.852 178.887 179.644];
FE_1mod_100_fps = [5.2374 5.3219 5.2675 5.6039];
FE_1mod_1000_fps = [5.5741 5.5293 5.5901 5.5665];

FE_2mod_100_t = [64.5284 63.575 58.8694 60.2576];
FE_2mod_1000_t = [606.379 607.512 606.121 608.023];
FE_2mod_100_fps = [1.5497 1.5729 1.6986 1.6505];
FE_2mod_1000_fps = [ 1.6491 1.6460 1.6498 1.6446];

%% 100 iterations

FE_1mod_100_t_m = mean(FE_1mod_100_t)
FE_1mod_100_fps_m = mean(FE_1mod_100_fps)

FE_2mod_100_t_m = mean(FE_2mod_100_t)
FE_2mod_100_fps_m = mean(FE_2mod_100_fps)

mod_100_t_m = mean(mod_100_t)
mod_100_fps_m = mean(mod_100_fps)

mod2_100_t_m = mean(mod2_100_t)
mod2_100_fps_m = mean(mod2_100_fps)

%% 1000 iterations

FE_1mod_1000_t_m = mean(FE_1mod_1000_t)
FE_1mod_1000_fps_m = mean(FE_1mod_1000_fps)

FE_2mod_1000_t_m = mean(FE_2mod_1000_t)
FE_2mod_1000_fps_m = mean(FE_2mod_1000_fps)

mod_1000_t_m = mean(mod_1000_t)
mod_1000_fps_m = mean(mod_1000_fps)

mod2_1000_t_m = mean(mod2_1000_t)
mod2_1000_fps_m = mean(mod2_1000_fps)

%% STD ! 100 iterations

FE_1mod_100_t_std = std(FE_1mod_100_t)
FE_1mod_100_fps_std = std(FE_1mod_100_fps)

FE_2mod_100_t_std = std(FE_2mod_100_t)
FE_2mod_100_fps_std = std(FE_2mod_100_fps)

mod_100_t_mstd = std(mod_100_t)
mod_100_fps_mstd = std(mod_100_fps)

mod2_100_t_mstd = std(mod2_100_t)
mod2_100_fps_mstd = std(mod2_100_fps)

%% STD ! 1000 iterations

FE_1mod_1000_t_mstd = std(FE_1mod_1000_t)
FE_1mod_1000_fps_mstd = std(FE_1mod_1000_fps)

FE_2mod_1000_t_mstd = std(FE_2mod_1000_t)
FE_2mod_1000_fps_mstd = std(FE_2mod_1000_fps)

mod_1000_t_mstd = std(mod_1000_t)
mod_1000_fps_mstd = std(mod_1000_fps)

mod2_1000_t_mstd = std(mod2_1000_t)
mod2_1000_fps_mstd = std(mod2_1000_fps)

