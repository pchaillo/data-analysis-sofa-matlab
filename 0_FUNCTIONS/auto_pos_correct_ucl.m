function tab_new = auto_pos_correct_ucl(tab,pos_i_d)
% corrige automatiquement les positions pour les remettre dans le
% referentiel du robot

% pos_r = data_struct{1,1};
% pres = data_struct{1,2};

% conversion en mm
% pos = abs(10*pos_r);

pos = tab;
pos_i = tab(1,:);

% décalage à appliquer pour se remmettre par translation dans le repère du robo
dec = pos_i_d - pos_i; 

pos = pos + dec ;

tab_new = pos;
