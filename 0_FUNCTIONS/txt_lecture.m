function data_struct = txt_lecture(nom_txt)

chemin_txt = choix_chemin('raw_data',nom_txt);
% Chargement du fichier txt
fiche = fopen(chemin_txt) ;
% raw_dat = load(chemin_txt) ;

% Récupération des informations de ces fichiers
out = textscan(fiche,'%s%s','delimiter','::');

raw_dat = out{1, 1}  ;

l = length(raw_dat);

ind = 0;
for i = 1:2 : l-1
    ind = ind + 1;
    Positions_r(ind) = {raw_dat{i, 1} };
    Pressions(ind) = str2double(raw_dat{i+1, 1}) ;
end

for i = 1 : length(Positions_r)
    Positions(i,:) = str2num(Positions_r{1, i});
end
    
data_struct = {Positions, Pressions};

