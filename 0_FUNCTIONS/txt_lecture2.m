function data_struct = txt_lecture2(nom_txt,folder_name,nb_groupe)

chemin_txt = choix_chemin(folder_name,nom_txt);
% Chargement du fichier txt
fiche = fopen(chemin_txt) ;
% raw_dat = load(chemin_txt) ;

% Récupération des informations de ces fichiers
out = textscan(fiche,'%s%s','delimiter','::');


raw_dat = out{1, 1}  ;

l = length(raw_dat);

ind = 0;
for i = 1:nb_groupe : l-1
    ind = ind + 1;
    for j = 0 : nb_groupe -1
        Groupe_r{j+1}(ind) = {raw_dat{i+j, 1} };
%         Positions_r(ind) = {raw_dat{i, 1} };
%         Pressions_r(ind) = {raw_dat{i+1, 1}} ;
    end
end

for i = 1 : length(Groupe_r{1,1})
    for j = 0 : nb_groupe -1
        Groupe{j+1}(i,:) = str2num(Groupe_r{j+1}{1,i});
        %     Positions(i,:) = str2num(Positions_r{1, i});
        %     Pressions(i,:) = str2num(Pressions_r{1, i});
    end
end

data_struct = Groupe;

