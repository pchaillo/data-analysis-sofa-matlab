function ind_tab = changement_detector(tab)

% Fonction qui renvoie le tableau des derniers indices avant les
% changements de pressions (positions stabilisés)

l = length(tab);
% changement_ind_tab = []; % tableau du dernier indice avant le changement
% de pressions (position stabilisé)
u = 0;
for i = 1 : l -1
    if tab(i) ~= tab(i+1)
        u = u +1;
        ind_tab(u) = i;
    end
end