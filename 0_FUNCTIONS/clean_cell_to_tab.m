% vieille fonction, je ne me rappelle plus de son utilité

function tab_out = clean_cell_to_tab(tab)

l = length(tab);
for i = 1 : l
    p = tab(i);
    p2 = p{1,1}(2:end-1);
    p3 = strsplit(p2,' ');
    emptyCells = cellfun('isempty', p3); % supprime les cases vides
    p3(emptyCells) = [];
    tab_out(i,:) = str2double(p3);
end