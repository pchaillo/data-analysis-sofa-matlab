function data = folder_import(nom_dossier,nom_fichier,column_nb)

% Fonction pour importer tous les données dans un dossier dans un
% sous-dossier => redécomposer en sous-fonctions ?

% column_nb => variable pour le découpage des différentes colonnes
% Mieux vaut trop que pas assez (crée des tableaux vides, alors qu'un
% manque de colonne va les mélanger 

format = '';
for i = 1 : column_nb 
    format = strcat(format,'%s');
end

path_name = strcat(nom_dossier,'/',nom_fichier);

listeFichiersCSV = dir(strcat('./',nom_dossier,'/',nom_fichier,'/*.csv')) ;

% for i = 1 : length(listeFichiersCSV)
%     data_struct{i} = csv_lecture( listeFichiersCSV(i).name);
% end

for i = 1 : length(listeFichiersCSV)
    %     chemin_csv{i} = choix_chemin(nom_fichier,listeFichiersCSV(i).name);
    chemin_csv{i} = choix_chemin(path_name,listeFichiersCSV(i).name);
    % Chargement du fichier csv
    fiche{i} = fopen(chemin_csv{i}) ;
    
    % Récupération des informations de ces fichiers
    out{i} = textscan(fiche{i},format,'delimiter',',');
    
end

data = out;

return 