function data_new = auto_pos_correct(data_struct,pos_i_d)
% corrige automatiquement les positions pour les remettre dans le
% referentiel du robot

pos_r = data_struct{1,1};
pres = data_struct{1,2};

% conversion en mm
pos = abs(10*pos_r);

pos_i = pos(1,:);

% décalage à appliquer pour se remmettre par translation dans le repère du robo
dec = pos_i_d - pos_i; 

pos = pos + dec ;

data_new{1,1} = pos;
data_new{1,2} = pres;