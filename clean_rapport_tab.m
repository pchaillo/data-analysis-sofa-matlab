function tab = clean_rapport_tab(tab)

ind = 0;
for i = 1 : length(tab)
    if tab(i,2) < 0 || tab(i,2) == 0% sipression négative, retirer la valeur
        ind = ind + 1;
        ind_tab(ind) = i;
    end
end

if exist('ind_tab')
    tab(ind_tab,:) = [];
end
% supprimer après, pour pas que le tableau soit moins long aque lq longueur
% qu'on donne à la boucle for
