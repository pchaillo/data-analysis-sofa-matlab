function print_all_data2(listeFichiersTxt,data_struct,target_folder,deb_nom,nb_groupe,nb_valeur,recti_tab)

for i = 1 : length(data_struct)
    nom_r1 = listeFichiersTxt(i).name;
    nom_r2 = strsplit(nom_r1,'.');
    nom_r3 = nom_r2{1, 1} ;
    nom = strcat(deb_nom,nom_r3);
    nom  = choix_chemin(target_folder,nom);
    
    ind = 0 ;
    for j = 1 : nb_groupe
        for k = 1 : nb_valeur(j)
            ind = ind + 1;
            tab(:,ind) = data_struct{1, i}{1, j}(:,k); % distance a gauche
%         tab(:,2) = data_struct{1, i}{1, 1}(:,2); % distance a gauche
%         tab(:,3) = data_struct{1, i}{1, 1}(:,3); % distance a gauche
%         tab(:,4) = data_struct{1, i}{1, 2}; % presison a droite
        end
    end
    
%     tab = clean_rapport_tab2(tab,nb_groupe,nb_valeur,recti_tab);
    tab = clean_rapport_tab3(tab,nb_groupe,nb_valeur,recti_tab);

    
    print_data2(tab,nom,nb_groupe,nb_valeur)
    
    clear tab
end