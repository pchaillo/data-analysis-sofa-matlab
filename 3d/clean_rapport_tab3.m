function tab = clean_rapport_tab3(tab,nb_groupe,nb_valeur,recti_tab)

% is2 = find(recti_tab == 2);
% if ~isempty(is2)
% 
% end

%% Création du masque pour ne tester que les valeurs souhaités
ind_msk = 0;
ind_tab_msk = 0;
compar_ind = 0;
for k = 1 : nb_groupe
    for l = 1 : nb_valeur(k)
        ind_msk = ind_msk + 1 ;
        if recti_tab(k) == 1
            ind_tab_msk = ind_tab_msk + 1;
            msk_ind(ind_tab_msk) = ind_msk;
        elseif recti_tab(k) == 2
            compar_ind = compar_ind + 1;
            compar_msk(compar_ind) = ind_msk;
        end
    end
end


%% Si une valeur de pression mesurée est nulle => supprimer la ligne relative à cette pression
ind = 0;
for i = 1 : length(tab)
    if sum( tab(i,msk_ind)==0 & tab(i,compar_msk)~= 0 )% sipression négative, retirer la valeur
        ind = ind + 1;
        ind_tab(ind) = i;
    end
end

if exist('ind_tab')
    tab(ind_tab,:) = [];
end
% supprimer après, pour pas que le tableau soit moins long aque lq longueur
% qu'on donne à la boucle for

u = 0;

