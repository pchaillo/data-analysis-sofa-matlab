function print_data2(tab,nom,nb_groupe,nb_valeur)
%imprime toutes les positions corrigées et les pressions relatives dans un
%tableau

% Création de l'identifiant d'impression
id_print_var = '';
for i = 1 : nb_groupe
    for j = 1 : nb_valeur(i)
        id_print_var = strcat(id_print_var, '%d,');
    end
end
id_print_var = id_print_var(1:end-1);
id_print_var = strcat(id_print_var, '\n');

%# write line-by-line
fid = fopen(strcat(nom,'.csv'),'wt');
for i=1:length(tab)
    fprintf(fid, id_print_var, tab(i,:));
end
fclose(fid);