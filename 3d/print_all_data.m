function print_all_data(listeFichiersTxt,data_struct)

for i = 1 : length(data_struct)
    nom_r1 = listeFichiersTxt(i).name;
    nom_r2 = strsplit(nom_r1,'.');
    nom_r3 = nom_r2{1, 1} ;
    nom = strcat('rapport_pos_',nom_r3);
    nom  = choix_chemin('rapport',nom);
    
    tab(:,1) = data_struct{1, i}{1, 1}(:,1); % distance a gauche
    tab(:,2) = data_struct{1, i}{1, 1}(:,2); % distance a gauche
    tab(:,3) = data_struct{1, i}{1, 1}(:,3); % distance a gauche
    tab(:,4) = data_struct{1, i}{1, 2}; % presison a droite
    
    tab = clean_rapport_tab(tab);
    
    print_data(tab,nom)
    
    clear tab
end