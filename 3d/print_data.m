function print_data(tab,nom)
%imprime toutes les positions corrigées et les pressions relatives dans un
%tableau

%# write line-by-line
fid = fopen(strcat(nom,'.csv'),'wt');
for i=1:length(tab)
    fprintf(fid, '%d,%d,%d,%d\n', tab(i,1),tab(i,2),tab(i,3),tab(i,4));
end
fclose(fid);