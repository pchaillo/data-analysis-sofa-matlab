
clc
clear all
close all

% Version du 17/06/2022 pchaillo

path(path,'2d')
% path(path,'3d')

%% Paramètres

% position initiale désirée
% pos_i_d = [0,0,58] ; % Pour stiff flop 
pos_i_d = [0,0,5] ; % Pour jacob

source_folder = 'raw_data3'; % name of the source folder (with the experimental data)

target_folder = 'rapport6' ; % name of the target folder (where will be store the corrected data
deb_nom = 'rapport_better_' ; % debut du nom des fichier csv générés

% attention, positions dans le 1er gourpe pour auto_pos_correct2.m
nb_valeur = [3 3 3]; % nb valeur par groupe
recti_tab = [0 1 2]; % [recti_tab] : Si le groupe est à 1, alors les lignes avec des valeurs nulles seront supprimées
% [recti_tab] : si le groupe est à 2, alors on ne teste si la valeur du
% groupe qui est à 1 est à 0 seulement pour les variable du goupe à 2
% non-nulle (comparaison entre pression mesurée et appliquée, si la
% pression appliquée est  nulle c'est ok d'en mesurer une nulle aussi)
% seulement avec clean_rapport_tab3
nb_groupe = length(nb_valeur); % nb de groupe de valeur (position, pression_mesurée, pression_commande etc.)
% variable nb_goupe non-indispensable, on pourrait s'en passer 

conv_fact = 100; % Facteur de conversion entre kPa et bar => mettre à 1 si toutes les unités sont identiques

%% Code 

var_nom_liste = strcat('./' , source_folder, '/*.txt'); % pour avoir le bon argumet pour la fonction dir
listeFichiersTxt = dir(var_nom_liste) ;

i = 1;
for i = 1 : length(listeFichiersTxt)
    data_struct_r{i} = txt_lecture2( listeFichiersTxt(i).name , source_folder,nb_groupe);
    data_struct{i} = auto_pos_correct2(data_struct_r{i},pos_i_d,conv_fact);
%     tab = data_struct{1,i}{1, 1};
%     data_struct{i}{1, 3} = tab_dist2d:(tab); % for 2d problem
end

% print_all_dist_pres(listeFichiersTxt,data_struct) % for 2d problem
print_all_data2(listeFichiersTxt,data_struct,target_folder,deb_nom,nb_groupe,nb_valeur,recti_tab) % enregistre les données après traitement dans des .csv

%% Utilisation / Interprétation des données

% 1 % CALCUL DU RAPPORT PRESSION / DISTANCE DE DEPLACEMENT
% % % n'apporte pas grand chose, rapport pression/position pas nécessairement
% % % linéaire

% l = length(data_struct);
% for i = 1 : l
%     pres_tab = data_struct{1, i}{1, 2};
%     dist_tab = data_struct{1, i}{1, 3};
%     for j = 1 : length(pres_tab)
%         rapport_tab{i}(j) = pres_tab(j)/dist_tab(j);
%     end
%     mean_rapport_tab(i) = mean(rapport_tab{1, i} ,'omitnan');
%     median_rapport_tab(i) = median(rapport_tab{1, i},'omitnan' );
% end
 
% 2 % CALCUL DE LA DIFF ENTRE PRESSION APPLIQUEE ET PRESSION MESUREE
l = length(data_struct);
for i = 1 : l
    pres_tab_mes = data_struct{1, i}{1, 2};
    pres_tab_com = data_struct{1, i}{1, 3};
    for j = 1 : length(pres_tab_mes)
%         rapport_tab{i}(j) = pres_tab(j)/dist_tab(j);
          diff_tab{i}(j) = pres_tab_mes(j) - pres_tab_com(j)*100; % *100 pour convertir les bar en kPa
    end
%     mean_rapport_tab(i) = mean(rapport_tab{1, i} ,'omitnan');
%     median_rapport_tab(i) = median(rapport_tab{1, i},'omitnan' );
    mean_diff(i) = mean(diff_tab{1, i} ,'omitnan'); % 1 moyenne par fichier
end