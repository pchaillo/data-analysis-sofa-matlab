function dist_tab = tab_dist2d_ucl(tab)

% en 2d, on ne prend les valeurs qu'n x et y

for i = 1 : length(tab)
    dist_tab(i) = sqrt( tab(i,1)^2 + tab(i,2)^2); % oh mince, tt est à refaire
end