function dist_tab = tab_dist2d(tab)

% en 2d, on ne prend les valeurs qu'n x et y

for i = 1 : length(tab)
    dist_tab(i) = sqrt( tab(i,2)^2 + tab(i,3)^2); % oh mince, tt est à refaire
end