function data_new = auto_pos_correct2(data_struct,pos_i_d,conv_fact)
% corrige automatiquement les positions pour les remettre dans le
% referentiel du robot

data_int = data_struct;

%% Suppression de la 1ère ligne (à l'initialisation, les valeurs du polhemus
% semblent fausses 
si = size(data_int);
for i = 1 : si(2)
    data_int{1,i}(1,:) = []
%     si2(i,:) = size(data_int{1,i})
end
% variable intermédiaire data_int non indispensable

%%
pos_r = data_int{1,1};
%pres = data_struct{1,2};
%reste = data_struct{1,2:end};

% conversion en mm
pos_r = abs(10*pos_r); % ATTENTION aux changements des paramètres polhemus ? (c'était un rapport de 10 pour l'exp avec le stiff flop

pos_i = pos_r(1,:);

% décalage à appliquer pour se remmettre par translation dans le repère du robo
dec = pos_i_d - pos_i; 

pos = pos_r + dec ;

data_new = data_int;
data_new{1,1} = pos;
data_new{1,3} = data_int{1,3}*conv_fact
%data_new{1,2} = pres;