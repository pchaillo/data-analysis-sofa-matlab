
data1 = data_struct{1};
data2 = data_struct{2};

pos1 = data1{2};
pos2 = data2{2};

pres1 = data1{4}{1,1};
pres2 = data2{4}{1,1};

% rapport pression chambre 1 et position effecteur en z
pos1_z = pos1(:,3);
pres1_1 = pres1(:,1);
compar1 = [pos1_z pres1_1];

pos2_z = pos2(:,3);
pres2_1 = pres2(:,1);
compar2 = [pos2_z pres2_1];

% for i = 1 : length(pos1)

