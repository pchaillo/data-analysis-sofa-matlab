clc
clear all
close all

path(path,'0_FUNCTIONS')

% Fonction pour plot les différentes positions (simulation, goal, recorded
% position, etc.)

open_loop = 0;

% UCl (old_data)
% nom_fichier = '2022-09-16 13:18:46';
% nom_fichier = '2022-09-16 16:13:25';

% close_loop
% nom_fichier = '2022-09-28 15:45:45';
% nom_fichier = '2022-09-28 16:24:55'; %  0.5 mm  error slow square
% nom_fichier = '2022-09-28 16:44:34';
% nom_fichier = '2022-09-28 17:14:29';
% nom_fichier = '2022-09-28 17:24:00';
% nom_fichier = '2022-09-28 17:31:39';
% nom_fichier = '2022-09-28 17:38:46';
% nom_fichier = '2022-09-29 11:24:20';
% nom_fichier = '2022-09-29 11:26:13'; % 0.9mm error slow circle 
% nom_fichier = '2022-09-29 14:34:41';
% nom_fichier = '2022-09-29 14:38:33';
% nom_fichier = '2022-09-29 15:58:31';
% nom_fichier = '2022-09-29 16:02:12';
% nom_fichier = '2022-09-29 16:08:30';
% nom_fichier = '2022-09-29 16:48:21';
% nom_fichier = '2022-09-29 16:52:45';
% nom_fichier = '2022-09-29 16:56:07';
% nom_fichier = '2022-09-29 17:03:51';
% nom_fichier = '2022-09-29 17:07:53';
% nom_fichier = '2022-09-29 17:12:24'; % 1.4 mm  error 
% nom_fichier = '2022-09-29 17:29:29';
% nom_fichier = '2022-09-29 17:21:13';
% nom_fichier = '2022-09-29 17:40:20';
% nom_fichier = '2022-09-29 17:42:41';
% nom_fichier = '2022-09-29 17:46:45';

nom_fichier = '2022-10-05-OL-20mm-rect';



% open_loop
% nom_fichier = '2022-09-28 17:06:01';


% nom_dossier
nom_dossier = 'ucl'
% nom_dossier = 'close_loop';
% nom_dossier = 'open_loop'; 
open_loop = 1;
% nom_dossier = 'old_data';

listeFichiersCSV = dir(strcat('./',nom_dossier,'/',nom_fichier,'/*.csv')) ;
column_nb = 2;
out = folder_import(nom_dossier,nom_fichier,column_nb);

if open_loop == 1
    desired_pos = out{1, 6}{1, 1};
    measured_pos = out{1, 1}{1, 1} ; 
    beam_pos = out{1, 4}{1, 1} ; 
    % sim_pos = out{1, 6}{1, 1} ; 
    param = out{1, 2};
    pressure = out{1,3}
else 
    desired_pos = out{1, 1}{1, 1};
    measured_pos = out{1, 2}{1, 1} ; 
    beam_pos = out{1, 5}{1, 1} ; 
    % sim_pos = out{1, 6}{1, 1} ; 
    param = out{1, 3};
end

param_nom = param{1, 1}  
param_value = param{1, 2}  



for i = 1 : length(desired_pos)
    desired_tab(i,:) = str2num(desired_pos{i, 1});
    measured_tab(i,:) = str2num(measured_pos{i, 1}) ;
    beam_tab(i,:) = str2num(beam_pos{i, 1});
end

figure()
hold on
plot3(beam_tab(:,1),beam_tab(:,2),beam_tab(:,3))
plot3(measured_tab(:,1),measured_tab(:,2),measured_tab(:,3))
plot3(desired_tab(:,1),desired_tab(:,2),desired_tab(:,3))
hold off

error = desired_tab-measured_tab;
error_dist =sqrt(error(:,1).^2+ error(:,2).^2+error(:,3).^2);
figure()
plot(error)
hold on
plot(error_dist)

mean_error = mean(error_dist(50:end));
max_error = max(error_dist(50:end));
min_error = min(error_dist(50:end));
median_error = median(error_dist(50:end));

[mean_error max_error min_error median_error]

t_total = out{1, 2}{1, 2}{end, 1}

%%%%% ANALYSIS

z_meas = measured_tab(:,3);
x_meas = measured_tab(:,1);
y_meas = measured_tab(:,2);

ind2 = find(x_meas == -35.11) ;

min_z = min(z_meas);
[I,J,V] = find(z_meas == min_z) ;

pression_temps = pressure{1, 1};
volume = pressure{1, 2}  ;

u = 0;
for i = 1 : length(pression_temps)
    if mod(i,2) ~= 0
        u = u + 1;
        pression_tab(u) = pression_temps(i);
    end
end

pression_voulue = pression_tab(I);
pos_voulue = measured_tab(I,:);

pression_voulue2 = pression_tab(ind2)
pos_voulue2 = measured_tab(ind2,:)





