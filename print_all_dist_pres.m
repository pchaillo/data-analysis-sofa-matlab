function print_all_dist_pres(listeFichiersTxt,data_struct)

for i = 1 : length(data_struct)
    nom_r1 = listeFichiersTxt(i).name;
    nom_r2 = strsplit(nom_r1,'.');
    nom_r3 = nom_r2{1, 1} ;
    nom = strcat('rapport_',nom_r3);
    nom  = choix_chemin('rapport',nom);
    
    tab(:,1) = data_struct{1, i}{1, 3}; % distance a gauche
    tab(:,2) = data_struct{1, i}{1, 2}; % presison a droite
    
    tab = clean_rapport_tab(tab);
    
    print_dist_pres(tab,nom)
    
    clear tab
end