clc
clear all
close all

path(path,'0_FUNCTIONS')

% Fonction pour comparer de différentes positions pour des pressions
% équivalentes

nom_dossier =  'pos_compare';
column_nb = 3;

dist_tab_1ch = pos_compar_fcn(nom_dossier,'05',column_nb)
dist_tab_2ch = pos_compar_fcn(nom_dossier,'07_2bis',column_nb)
dist_tab_3ch = pos_compar_fcn(nom_dossier,'06_elong',column_nb)

max_val = 11

mean_dist_1ch = mean(dist_tab_1ch(1:max_val));
median_dist_1ch = median(dist_tab_1ch(1:max_val));

mean_dist_2ch = mean(dist_tab_2ch(1:max_val));
median_dist_2ch = median(dist_tab_2ch(1:max_val));

mean_dist_3ch = mean(dist_tab_3ch(1:max_val));
median_dist_3ch = median(dist_tab_3ch(1:max_val));

pressure_tab = [0:10:190];


figure()
line_width = 3;
hold ON
plot_bis = plot(pressure_tab(1:max_val),dist_tab_1ch(1:max_val));
plot_ter = plot(pressure_tab(1:max_val),dist_tab_2ch(1:max_val));
plot_quad = plot(pressure_tab(1:max_val),dist_tab_3ch(1:max_val));
set(plot_bis,'linewidth',line_width);
set(plot_ter,'linewidth',line_width);
set(plot_quad,'linewidth',line_width);
xlabel('Applied pressure in kPa') 
ylabel('Error between the two models in mm') 
legend({'One camber actuated','Two chambers actuated','Three chambers actuated'},'Location','northwest')
hold OFF