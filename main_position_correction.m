
clc
clear all
close all

listeFichiersTxt = dir('./raw_data/*.txt') ;

pos_i_d = [0,0,58] ; % position initiale désirée

i = 1;
for i = 1 : length(listeFichiersTxt)
    data_struct_r{i} = txt_lecture( listeFichiersTxt(i).name);
    data_struct{i} = auto_pos_correct(data_struct_r{i},pos_i_d);
    tab = data_struct{1,i}{1, 1};
    data_struct{i}{1, 3} = tab_dist2d(tab);
end

% % % n'apporte pas grand chose, rapport pression/position pas nécessairement
% % % linéaire

% l = length(data_struct);
% for i = 1 : l
%     pres_tab = data_struct{1, i}{1, 2};
%     dist_tab = data_struct{1, i}{1, 3};
%     for j = 1 : length(pres_tab)
%         rapport_tab{i}(j) = pres_tab(j)/dist_tab(j);
%     end
%     mean_rapport_tab(i) = mean(rapport_tab{1, i} ,'omitnan');
%     median_rapport_tab(i) = median(rapport_tab{1, i},'omitnan' );
% end

print_all_dist_pres(listeFichiersTxt,data_struct)

