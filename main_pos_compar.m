clc
clear all
close all

path(path,'0_FUNCTIONS')

% Fonction pour comparer de différentes positions pour des pressions
% équivalentes

nom_dossier = 'pos_compare';
nom_fichier = '07_2';
column_nb = 3;

listeFichiersCSV = dir(strcat('./',nom_dossier,'/',nom_fichier,'/*.csv')) ;

out = folder_import(nom_dossier,nom_fichier, column_nb);

fem_pos = clean_data(out{1, 1}{1, 1} );
fem_pres = clean_data( out{1, 3}{1, 1});
beam_pres = clean_data(out{1, 2}{1, 1});
beam_pos = clean_data(out{1, 4}{1, 1});

fem_ind_tab = changement_detector(fem_pres(:,1));
beam_ind_tab = changement_detector(beam_pres(:,1));

for i = 1 : length(fem_ind_tab)
    fem_tab(i,:) = fem_pos(fem_ind_tab(i),:);
end

for i = 1 : length(beam_ind_tab)
    beam_tab(i,:) = beam_pos(beam_ind_tab(i),:);
end

for i = 1 : length(fem_tab)
    dist_tab(i) = pdist2(beam_tab(i,:),fem_tab(i,:));
end

max_val = 15

mean_dist = mean(dist_tab(1:max_val))
median_dist = median(dist_tab(1:max_val))

pressure_tab = [0:10:190];
figure()
plot(pressure_tab(1:max_val),dist_tab(1:max_val));